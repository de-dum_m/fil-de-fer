/*
** my_printf.h for my_printf in /home/de-dum_m/code/B1-Systeme_Unix/TP/my_printf
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Nov 13 11:28:26 2013 de-dum_m
** Last update Wed Nov 13 15:34:10 2013 de-dum_m
*/

#ifndef MY_PRINTF_H_
#define MY_PRINTF_H_

void	my_count_putchar(char c);
void	my_cout_putstr(char *str);
int	count_prints();
int	my_count_put_nbr(int nb);
int	my_cout_putnbr_base(unsigned int nbr, char *base);
void	print_modified_count(char nb);
int	my_put_unsigned_nbr(unsigned int nb);

#endif /* MY_PRINTF_H_ */
