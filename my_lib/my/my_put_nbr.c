/*
** my_put_nbr.c for my_put_nbr.c in /home/de-dum_m/rendu/Piscine-C-Jour_03
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Oct  2 18:24:13 2013 de-dum_m
** Last update Wed Nov 13 15:34:01 2013 de-dum_m
*/

void	print_modified(char nb)
{
  my_putchar(nb + 48);
}

int	my_put_nbr(int nb)
{
  if (nb < 0)
    {
      nb = - nb;
      my_putchar('-');
    }
  if (nb >= 10)
    {
      my_put_nbr(nb / 10);
      my_put_nbr(nb % 10);
    }
  else
    print_modified(nb);

  return (0);
}
