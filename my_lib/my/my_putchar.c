/*
** my_putchar.c for my_putchar.c in /home/de-dum_m/rendu/Piscine-C-lib/my
** 
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
** 
** Started on  Tue Oct  8 18:38:44 2013 de-dum_m
** Last update Tue Oct  8 18:39:16 2013 de-dum_m
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}
