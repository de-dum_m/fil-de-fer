/*
** my_strcpy.c for my_strcpy.c in /home/de-dum_m/rendu/Piscine-C-Jour_06
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Oct  7 08:42:36 2013 de-dum_m
** Last update Mon Oct 21 10:09:13 2013 de-dum_m
*/
char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i = i + 1;
    }
  return (dest);
}
