/*
** my_strncpy.c for my_strncpy.c in /home/de-dum_m/rendu/Piscine-C-Jour_06
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Oct  7 09:01:53 2013 de-dum_m
** Last update Wed Nov 13 15:32:28 2013 de-dum_m
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (i < n)
    {
      dest[i] = src[i];
      i = i + 1;
    }
  return (dest);
}
