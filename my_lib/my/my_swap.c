/*
** my_swap.c for my_swap.c in /home/de-dum_m/rendu/Piscine-C-Jour_04
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Oct  3 08:51:19 2013 de-dum_m
** Last update Sun Dec  1 16:19:42 2013 de-dum_m
*/

int	my_swap(char **a, char **b)
{
  char	*c;

  c = *a;
  *a = *b;
  *b = c;
  return (0);
}
