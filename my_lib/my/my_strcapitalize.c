/*
** my_strupcase.c for my_strupcase.c in /home/de-dum_m/rendu/Piscine-C-Jour_06/ex_07
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Oct  7 17:02:48 2013 de-dum_m
** Last update Wed Nov 13 15:33:09 2013 de-dum_m
*/

char	*my_strcapitalize(char *str)
{
  int	i;

  i = 1;
  if (str[0] >= 97 && str[0] <= 122)
    str[0] = str[0] - 32;
  while (str[i] != '\0')
    {
      if (str[i - 1] == ' ' || str[i - 1] == '+' || str[i - 1] == '-')
	{
	  if (str[i] >= 97 && str[i] <= 122)
	    str[i] = str[i] - 32;
	}
      else if (str[i] <= 90 && str[i] >= 65 )
	str[i] = str[i] + 32;
      i = i + 1;
    }
  return (str);
}
