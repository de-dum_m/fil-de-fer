/*
** my_count_putchar.c for my_printf in /home/de-dum_m/code/B1-Systeme_Unix/TP/my_printf
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Nov 13 10:36:35 2013 de-dum_m
** Last update Wed Nov 13 10:58:39 2013 de-dum_m
*/

int	count_prints()
{
  static int	prints;

  prints = prints + 1;
  return (prints);
}

void	my_count_putchar(char c)
{
  write(1, &c, 1);
  count_prints();
}

void	my_count_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_count_putchar(str[i]);
      i = i + 1;
    }
}
