/*
** my_strcat.c for my_strcat.c in /home/de-dum_m/rendu/Piscine-C-Jour_07/ex_01
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue Oct  8 15:36:05 2013 de-dum_m
** Last update Sun Oct 20 21:32:17 2013 de-dum_m
*/

char	*my_strncat(char *dest, char *src, int nb)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (dest[i] != '\0')
    i = i + 1;
  while (j <= nb && src[j] != '\0')
    {
      dest[i] = src[j];
      i = i + 1;
      j = j + 1;
    }
  dest[i] = '\0';
  return (dest);
}
