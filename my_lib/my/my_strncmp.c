/*
** my_strcmp.c for my_strcmp.c in /home/de-dum_m/rendu/Piscine-C-Jour_06/ex_05
** 
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
** 
** Started on  Mon Oct  7 15:41:39 2013 de-dum_m
** Last update Mon Oct  7 16:55:09 2013 de-dum_m
*/
int	my_strncmp(char *s1, char *s2, int n);

int	my_strncmp(char *s1, char *s2, int n)
{
  int	 i;

  i = 0;
  while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0' && i < n)
    i = i + 1;
  if (i == n)
    return (0);
  if (s1[i] < s2[i])
    return (-1);
  return (1);
}
