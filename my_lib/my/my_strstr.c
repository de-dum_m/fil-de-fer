/*
** test.c for my_strstr.c in /home/de-dum_m/rendu/Piscine-C-Jour_06/ex_04
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Oct  7 14:01:13 2013 de-dum_m
** Last update Sun Oct 20 21:33:15 2013 de-dum_m
*/
char    *my_strstr(char *str, char *to_find)
{
  int	i;
  int	j;

  i = 0;
  while (str[i] != '\0')
    {
      j = 0;
      while (str[i + j] == to_find[j])
	{
	  j = j + 1;
	  if (to_find[j] == '\0')
	    return (to_find);
	}
      i = i + 1;
    }
  return (0);
}
