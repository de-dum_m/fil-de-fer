/*
** my_getnbr.c for my_getnbr in /home/de-dum_m
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Oct 20 12:58:24 2013 de-dum_m
** Last update Mon Oct 21 15:37:52 2013 de-dum_m
*/

int     neg_or_not(char *str)
{
  int   i;
  int   neg;

  neg = 1;
  i = 0;
  while (str[i] == '-' || str[i] == '+')
    {
      if (str[i] == '-')
        neg = neg + 1;
      i = i + 1;
    }
  if (neg % 2 == 0)
    return (-1);
  else
    return (1);
}

int	my_getnbr(char *str)
{
  int	i;
  int	num;
  int	neg;

  num = 0;
  i = 0;
  neg = neg_or_not(str);
  while ((str[i] >= '0' && str[i] <= '9') || (str[i] == '+' || str[i] == '-'))
    {
      if (num <= num * 10 && num >= 0 && str[i] >= '0' && str[i] <= '9')
	{
	  num = num * 10;
	  num = num + (str[i] - 48);
	}
      else if (num < num * 10 || num < 0)
	return (0);
      i = i + 1;
    }
  return (num * neg);
}
