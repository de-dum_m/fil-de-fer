/*
** my_is_alpha.c for my_isalpha.c in /home/de-dum_m/rendu/Piscine-C-Jour_06/ex_10
** 
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
** 
** Started on  Mon Oct  7 18:21:30 2013 de-dum_m
** Last update Mon Oct  7 21:10:27 2013 de-dum_m
*/

int	my_str_isprintable(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < 32)
	{
	return (0);
	}
	i = i + 1;
    }
  return (1);
}
