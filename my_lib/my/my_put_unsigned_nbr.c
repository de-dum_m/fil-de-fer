/*
** my_put_nbr.c for my_put_nbr.c in /home/de-dum_m/rendu/Piscine-C-Jour_03
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Oct  2 18:24:13 2013 de-dum_m
** Last update Wed Nov 13 12:32:26 2013 de-dum_m
*/

#include "my_printf.h"

void	print_modified_count(char nb)
{
  my_count_putchar(nb + 48);
}

int	my_put_unsigned_nbr(unsigned int nb)
{
  if (nb < 0)
    {
      nb = - nb;
      my_count_putchar('-');
    }
  if (nb >= 10)
    {
      my_count_put_nbr(nb / 10);
      my_count_put_nbr(nb % 10);
    }
  else
    print_modified_count(nb);
  return (0);
}
