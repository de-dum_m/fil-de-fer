/*
** my_strupcase.c for my_strupcase.c in /home/de-dum_m/rendu/Piscine-C-Jour_06/ex_07
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Oct  7 17:02:48 2013 de-dum_m
** Last update Wed Nov 13 15:32:22 2013 de-dum_m
*/

char	*my_strupcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] >= 97 && str[i] <= 122)
	str[i] = str[i] - 32;
      i = i + 1;
    }
  return (str);
}
