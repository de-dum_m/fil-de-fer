/*
** my_power_rec.c for my_power_rec.c in /home/de-dum_m/rendu/Piscine-C-Jour_05
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Oct  4 15:11:55 2013 de-dum_m
** Last update Sun Oct 20 21:31:23 2013 de-dum_m
*/

int	my_power_rec(int nb, int power)
{
  int	res;

  res = nb;
  return (do_the_rec(nb, power, res));
}

int	do_the_rec(int nb, int power, int res)
{
  if (power > 1 && res <= res * nb && res * nb >= 0)
    {
      res = res * nb;
      do_the_rec(nb, (power - 1), res);
    }
  else if (power == 1)
    return (res);
  else if (power == 0)
    return (1);
  else if (res * nb < nb || res * nb < 0)
    return (0);
}
