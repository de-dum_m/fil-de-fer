/*
** my_evil_str.c for my_evil_str in /home/de-dum_m/rendu/Piscine-C-Jour_04
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Oct  3 10:46:36 2013 de-dum_m
** Last update Sun Oct 20 21:34:06 2013 de-dum_m
*/
char	*my_revstr(char *str);
char	*reverse_magic(int i, int j, char *str);

char	*my_revstr(char *str)
{
  int   i;
  int	j;

  j = 0;
  i = 0;
  while (str[i] != '\0')
    i = i + 1;
  return (reverse_magic(i, j, str));
}

char	*reverse_magic(int i, int j, char *str)
{
  char	reved[i];

  while (j <= i + 1)
    {
      reved[j] = str[i - j - 1];
      j = j + 1;
    }
  j = 0;
  while (j < i)
    {
      str[j] = reved[j];
      j = j + 1;
    }
  return (str);
}
