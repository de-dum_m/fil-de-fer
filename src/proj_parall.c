/*
** proj_parall.c for fdf in /home/de-dum_m/code/B1-igraph/TP/fdf
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Dec  2 14:04:05 2013 de-dum_m
** Last update Sun Dec  8 12:13:25 2013 de-dum_m
*/

#include "mlx_p.h"

int		make_limit_nb(int nb)
{
  static int	lim;

  if (nb != -1)
    lim = nb;
  else
    return (lim);
  return (0);
}

int	recentre(t_point *pt, t_mlx *mx)
{
  int	decalagex;
  int	decalagey;

  decalagex = WINSIZE / 2;
  decalagey = (WINSIZE / 2) - (WINSIZE / 4);
  pt->x = decalagex + pt->x;
  pt->y = decalagey + pt->y;
}

int	proj_iso(t_point *pt, t_mlx *mx)
{
  int	x;
  int	y;
  float	cte;
  float cte2;

  x = pt->x;
  y = pt->y;
  cte = 0.5;
  cte2 = 0.5;
  pt->x = (x * cte) - (cte2 * y);
  pt->y = (pt->z + (((cte / 2) * x) + ((cte2 / 2) * y)));
  recentre(pt, mx);
}
