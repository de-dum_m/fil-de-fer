/*
** get_next_line.c for get_next_line in /home/de-dum_m/code/B1-C-Prog_Elem/get_next_line
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Nov 15 09:15:43 2013 de-dum_m
** Last update Thu Dec  5 20:59:44 2013 de-dum_m
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "get_next_line.h"

char		*alldone(char *line, int len)
{
  static int	laps;

  if (len == 0)
    return (NULL);
  if (laps == 0)
    {
      laps = 1;
      return (line);
    }
  else
    return (NULL);
}

char   *my_realloc(char *line, int len)
{
  int	i;
  char	*bigger_line;

  i = 0;
  if (len == 0 && ((line = malloc(BUFFSIZE + 1)) == NULL))
    return (NULL);
  if (len == 0)
    return (line);
  if ((bigger_line = malloc(len + BUFFSIZE + 1)) == NULL)
    return (NULL);
  while (i <= len)
    {
      bigger_line[i] = line[i];
      i = i + 1;
    }
  bigger_line[i] = '\0';
  free(line);
  return (bigger_line);
}

char		*get_next_line(const int fd)
{
  static int	i;
  static char	buffer[BUFFSIZE];
  static int	j;
  static int	red;
  static char	*line;

  if (i == 0 && (red = read(fd, buffer, BUFFSIZE)) == 0)
    return (alldone(line, j));
  if ((line = my_realloc(line, j)) == NULL)
    return (NULL);
  while (i < red)
    {
      if (buffer[i] == '\n')
	{
	  i = i + 1;
	  line[j] = '\0';
	  j = 0;
	  return (line);
	}
      line[j] = buffer[i];
      i = i + 1;
      j = j + 1;
    }
  i = 0;
  get_next_line(fd);
}
