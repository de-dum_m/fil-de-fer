/*
** can_u_draw.c for fdf in /home/de-dum_m/code/B1-igraph/TP/tp2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Nov 18 09:19:15 2013 de-dum_m
** Last update Sun Dec  8 12:08:37 2013 de-dum_m
*/

#include <math.h>
#include <mlx.h>
#include "mlx_p.h"

t_color		get_img_pix_col(int x, int y, t_mlx *mx)
{
  int		i;
  t_color	pix_col;

  i = y * mx->img_sizeline + x * (mx->bpp / 8);
  pix_col.rgb[0] = mx->img_data[i];
  pix_col.rgb[1] = mx->img_data[i + 1];
  pix_col.rgb[2] = mx->img_data[i + 2];
  if (mx->bpp / 8 == 4)
    pix_col.rgb[3] = mx->img_data[i + 3];
  return (pix_col);
}

int	draw_cas_un(t_mlx *mx)
{
  int	x;

  if (mx->pt1->x > mx->pt2->x)
    swap_int(mx->pt1, mx->pt2);
  x = mx->pt1->x;
  while (x <= mx->pt2->x)
    {
      my_pixel_put_to_img(x, mx->pt1->y
			  + ((mx->pt2->y - mx->pt1->y) * (x - mx->pt1->x))
			  / (mx->pt2->x - mx->pt1->x), mx);
      x = x + 1;
    }
  return (0);
}

int	draw_cas_deux(t_mlx *mx)
{
  int	y;

  if (mx->pt1->y > mx->pt2->y)
    swap_int(mx->pt1, mx->pt2);
  y = mx->pt1->y;
  while (y <= mx->pt2->y)
    {
      my_pixel_put_to_img(mx->pt1->x
			   + ((mx->pt2->x - mx->pt1->x) * (y - mx->pt1->y))
			  / (mx->pt2->y - mx->pt1->y), y, mx);
      y = y + 1;
    }
  return (0);
}

int	draw_straight_line(t_mlx *mx)
{
  if (mx->pt1->x == mx->pt2->x)
    {
      if (mx->pt1->y > mx->pt2->y)
	swap_int(mx->pt1, mx->pt2);
      while (mx->pt1->y < mx->pt2->y)
	{
	  my_pixel_put_to_img(mx->pt1->x, mx->pt1->y, mx);
	  mx->pt1->y = mx->pt1->y + 1;
	}
    }
  else
    {
      if (mx->pt1->x > mx->pt2->x)
	swap_int(mx->pt1, mx->pt2);
      while (mx->pt1->x < mx->pt2->x)
	{
	  my_pixel_put_to_img(mx->pt1->x, mx->pt1->y, mx);
	  mx->pt1->x = mx->pt1->x + 1;
	}
    }
}

int	what_case(t_mlx *mx)
{
  if (mx->pt1->x == mx->pt2->x && mx->pt1->y == mx->pt2->y)
      return (0);
  if (mx->pt1->x == mx->pt2->x || mx->pt1->y == mx->pt2->y)
    draw_straight_line(mx);
  else if (((mx->pt2->x - mx->pt1->x) >= (ABS(mx->pt2->y - mx->pt1->y))
	   || (mx->pt2->x - mx->pt1->x) >= (ABS(mx->pt2->y - mx->pt1->y)))
	   && mx->pt1->x <= mx->pt2->x)
    draw_cas_un(mx);
  else
    draw_cas_deux(mx);
  return (0);
}
