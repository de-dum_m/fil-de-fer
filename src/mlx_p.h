/*
** mlx_p.h for hellp in /home/de-dum_m/code/B1-igraph/TP
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Nov  8 11:34:19 2013 de-dum_m
** Last update Sun Dec  8 12:16:11 2013 de-dum_m
*/

#ifndef MLX_P_H_
#define MLX_P_H_

#define ABS(a) (((a) < 0) ? ((a) * -1) : (a))
#define WINSIZE 1000
#define HQ 0.3

typedef union	u_color
{
  unsigned int	col;
  unsigned char	rgb[4];
}		t_color;

typedef struct	s_point
{
  int	x;
  int	y;
  int	z;
}		t_point;

typedef struct	s_mlx
{
  void		*mlx;
  void		*win;
  void		*img;
  char		*img_data;
  int		img_sizeline;
  int		bpp;
  int		endian;
  int		size;
  float		heigh_x;
  t_point	*pt1;
  t_point	*pt2;
  char		*file;
}		t_mlx;

int	draw_cas_un(t_mlx *mx);
int	draw_cas_deux(t_mlx *mx);
int	what_case(t_mlx *mx);
int	my_pixel_put_to_img(int x, int y, t_mlx *mx);
int	proj_iso(t_point *pt, t_mlx *mx);
char	*get_next_line(int fd);
int	expose_event(t_mlx *mx);
int	key_event(int key, t_mlx *mx);
int	make_the_grid(char *file, t_mlx *mx);
int	make_limit_nb(int nb);
int	get_nb_at_index(char *str, int *i);
int	draw_line(int x, int y, int *line1, t_mlx *mx);
int	draw_col(int x, int y, int *line1, int *line2, t_mlx *mx);
int	*make_line(char *str);

#endif /* MLX_P_H_ */
