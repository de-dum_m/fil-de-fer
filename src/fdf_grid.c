/*
** fdf_grid.c for fdf in /home/de-dum_m/code/B1-igraph/TP/fdf
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue Dec  3 17:20:41 2013 de-dum_m
** Last update Sun Dec  8 12:13:13 2013 de-dum_m
*/

#include <stdlib.h>
#include <mlx.h>
#include "mlx_p.h"

t_mlx	*make_img(t_mlx *mx)
{
  int	bpp;
  int	endian;

  if (((mx = malloc(sizeof(t_mlx))) == NULL)
      || (mx->pt1 = malloc(sizeof(t_point))) == NULL
      || (mx->pt2 = malloc(sizeof(t_point))) == NULL
      || (mx->mlx = mlx_init()) == NULL
      || (mx->win = mlx_new_window(mx->mlx,
				   WINSIZE, WINSIZE, "IMG test")) == 0)
    return (0);
  return (mx);
}

int		draw_col(int x, int y, int *line1, int *line2, t_mlx *mx)
{
  mx->pt1->x = x * mx->size;
  mx->pt1->y = y * mx->size;
  mx->pt1->z = line1[x] * mx->heigh_x;
  mx->pt2->x = x * mx->size;
  mx->pt2->y = (y - 1) * mx->size;
  mx->pt2->z = line2[x] * mx->heigh_x;
  proj_iso(mx->pt1, mx);
  proj_iso(mx->pt2, mx);
  what_case(mx);
  return (0);
}

int		draw_line(int x, int y, int *line1, t_mlx *mx)
{
  mx->pt1->x = (x - 1) * mx->size;
  mx->pt1->y = y * mx->size;
  mx->pt1->z = line1[x - 1] * mx->heigh_x;
  mx->pt2->x = x * mx->size;
  mx->pt2->y = y * mx->size;
  mx->pt2->z = line1[x] * mx->heigh_x;
  proj_iso(mx->pt1, mx);
  proj_iso(mx->pt2, mx);
  what_case(mx);
  return (0);
}

int	main(int ac, char **av)
{
  t_mlx	*mx;

  if (ac == 2)
    {
      if ((mx = make_img(mx)) == 0)
	return (0);
      mx->file = av[1];
      mx->size = 1;
      mx->heigh_x = -0.1;
      if (make_the_grid(av[1], mx) == 0)
	return (0);
      mlx_expose_hook(mx->win, &expose_event, mx);
      mlx_key_hook(mx->win, &key_event, mx);
      mlx_loop(mx->mlx);
    }
  return (0);
}
