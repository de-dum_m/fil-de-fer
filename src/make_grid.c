/*
** make_grid.c for fdf in /home/de-dum_m/code/B1-igraph/TP/fdf
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Dec  8 11:51:01 2013 de-dum_m
** Last update Sun Dec  8 14:46:51 2013 de-dum_m
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <mlx.h>
#include "mlx_p.h"

int	get_nb_at_index(char *str, int *i)
{
  int	res;

  res = 0;
  while (str[*i] >= '0' && str[*i] <= '9')
    {
      res = (res * 10) + (str[*i] - 48);
      *i = *i + 1;
    }
  *i = *i - 1;
  return (res);
}

int	line_len(char *str)
{
  int	len;
  int	i;

  i = 0;
  len = 0;
  if (str == NULL)
    return (0);
  while (str[i])
    {
      if (str[i] >= '0' && str[i] <= '9')
	len = len + 1;
      else if ((str[i] < '0' || str[i] > '9') && str[i] != ' ')
	return (0);
      i = i + 1;
    }
  return (len);
}

int	*make_line(char *str)
{
  int	*line;
  int	i;
  int	j;
  int	len;

  i = 0;
  j = 0;
  if ((len = line_len(str)) == 0)
    return (0);
  if ((line = malloc(sizeof(int) * (len + 2))) == NULL)
    return (0);
  while (str[i])
    {
      if (str[i] >= '0' && str[i] <= '9')
	{
	  line[j] = get_nb_at_index(str, &i);
	  j = j + 1;
	}
      i = i + 1;
    }
  make_limit_nb(j);
  free(str);
  return (line);
}

int		get_grid(int fd, t_mlx *mx)
{
  int		*line1;
  int		*line2;
  int		i;
  int		j;
  int		len;

  j = 0;
  if (j == 0 && ((line1 = make_line(get_next_line(fd))) == 0))
    return (0);
  while ((line2 = make_line(get_next_line(fd))) != 0)
  {
    i = 0;
    while (i < make_limit_nb(-1))
      {
	draw_col(i, j, line2, line1, mx);
	draw_line(i, j, line2, mx);
	i = i + 1;
      }
    j = j + 1;
    line1 = line2;
  }
  free(line1);
  free(line2);
  mlx_put_image_to_window(mx->mlx, mx->win, mx->img, 0, 0);
  return (1);
}

int	make_the_grid(char *file, t_mlx *mx)
{
  int	fd;

  if ((fd = open(file, O_RDWR)) == -1
      || (mx->img = mlx_new_image(mx->mlx, WINSIZE, WINSIZE)) == 0
      || (mx->img_data =
	  mlx_get_data_addr(mx->img, &mx->bpp,
			    &mx->img_sizeline, &mx->endian)) == NULL)
    return (0);
  if (get_grid(fd, mx) == 0)
    return (0);
  close(fd);
  return (1);
}
