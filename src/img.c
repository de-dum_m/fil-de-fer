/*0
** img.c for  in /home/de-dum_m/code/B1-igraph/TP/tp2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Nov 14 16:41:15 2013 de-dum_m
** Last update Sun Dec  8 11:57:11 2013 de-dum_m
*/

#include <mlx.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "mlx_p.h"

void		swap_int(t_point *pt1, t_point *pt2)
{
  t_point	tmp;

  tmp =  *pt1;
  *pt1 = *pt2;
  *pt2 = tmp;
}

int		my_pixel_put_to_img(int x, int y, t_mlx *mx)
{
  t_color	color;
  int		i;
  int		j;

  if (x > WINSIZE - 1 || x < 0)
    return (0);
  if (y > WINSIZE || y < 0)
    return (0);
  if (ABS(mx->pt1->z) > 50 || ABS(mx->pt2->z) > 50)
    color.col = mlx_get_color_value(mx->mlx, 0xFFFFFF);
  else if (ABS(mx->pt1->z) > 20 || ABS(mx->pt2->z) > 20)
    color.col = mlx_get_color_value(mx->mlx, 0x00FF00);
  else if (ABS(mx->pt1->z) > 9 || ABS(mx->pt2->z) > 9)
    color.col = mlx_get_color_value(mx->mlx, 0xFFFF00);
  else
    color.col = mlx_get_color_value(mx->mlx, 0x0000FF);
  i = y * mx->img_sizeline;
  j = 4 * x;
  i = i + j;
  i = ABS(i);
  mx->img_data[i] = color.rgb[0];
  mx->img_data[i + 1] = color.rgb[1];
  mx->img_data[i + 2] = color.rgb[2];
  mx->img_data[i + 3] = color.rgb[3];
  return (0);
}

int	expose_event(t_mlx *mx)
{
  mlx_put_image_to_window(mx->mlx, mx->win, mx->img, 0, 0);
  return (0);
}

int	key_event(int key, t_mlx *mx)
{
  mlx_do_key_autorepeaton(mx->mlx);
  if (key == 65307)
    exit(0);
  else if (key == 65362)
    {
      mx->size = mx->size + mx->size;
      make_the_grid(mx->file, mx);
    }
  else if (key == 65364)
    {
      mx->size = mx->size - (mx->size / 2);
      make_the_grid(mx->file, mx);
    }
  else if (key == 65361)
    {
      mx->heigh_x = mx->heigh_x + 0.1;
      make_the_grid(mx->file, mx);
    }
  else if (key == 65363)
    {
      mx->heigh_x = mx->heigh_x - 0.1;
      make_the_grid(mx->file, mx);
    }
}
