/*
** get_next_line.h for get_next_line in /home/de-dum_m/code/B1-C-Prog_Elem/get_next_line
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Nov 15 10:29:22 2013 de-dum_m
** Last update Fri Nov 22 10:51:06 2013 de-dum_m
*/

#ifndef GET_NEXT_LINE_H_
#define GET_NEXT_LINE_H_

# define BUFFSIZE 2

char	*get_next_line(const int fd);

#endif /* GET_NEXT_LINE_H_ */
