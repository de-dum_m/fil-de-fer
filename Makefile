##
## Makefile for minilibx in /home/de-dum_m/code/B1-igraph/TP
## 
## Made by de-dum_m
## Login   <de-dum_m@epitech.net>
## 
## Started on  Fri Nov  8 09:32:19 2013 de-dum_m
## Last update Sun Dec  8 14:30:12 2013 de-dum_m
##

NAME	= fdf

SRC	= src/img.c \
	src/can_u_draw.c \
	src/proj_parall.c \
	src/fdf_grid.c \
	src/get_next_line.c \
	src/make_grid.c

SRO	= $(SRC:.c=.o)

SLIB	= -L/usr/lib64 -lmlx_$(HOSTTYPE) -L/usr/lib64/X11 -lXext -lX11

LIBDIR	= my_lib/my/

LIB	= -Lmy_lib/ -lmy -lm

HEADER	= -Imy_lib/my/my.h

CFLAGS	=

$(NAME)	: $(SRO)
	make -C $(LIBDIR)
	cc $(SRO) -o $(NAME) $(SLIB) $(LIB) $(HEADER)

all	: $(NAME)

clean	:
	make clean -C $(LIBDIR)
	rm -f $(SRO)

fclean	: clean
	make fclean -C $(LIBDIR)
	rm -f $(NAME)

re	: fclean all
